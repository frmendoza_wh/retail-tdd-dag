package co.williamhill.tdd.dag;

import static org.assertj.core.api.Java6Assertions.assertThat;

import java.util.Collection;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DagImplTest {

    private Node football;
    private Node competition;
    private Node player;
    private Node premierShip;
    private Node championLeague;
    private Node manCity;
    private Node manUtd;

    Dag<Node> unit;

    @BeforeEach
    public void setUp() {
        football = new NodeImpl("Football");
        competition = new NodeImpl("Competition");
        player = new NodeImpl("Player");
        premierShip = new NodeImpl("PremierShip");
        championLeague = new NodeImpl("Champion League");
        manCity = new NodeImpl("Man City");
        manUtd = new NodeImpl("Man Utd");

        football.addChild(competition);
        football.addChild(player);
        competition.addParent(football);
        player.addParent(football);
        competition.addChild(premierShip);
        competition.addChild(championLeague);
        premierShip.addParent(competition);
        championLeague.addParent(competition);
        premierShip.addChild(manCity);
        premierShip.addChild(manUtd);
        manCity.addParent(premierShip);
        manUtd.addParent(premierShip);
        championLeague.addChild(manUtd);
        manUtd.addParent(championLeague);

        unit = new DagImpl();
    }

    @Test
    void aLeafNodeShouldNotReturnDescendents() {
        Collection<Node> descendantsForNode = unit.getDescendantsForNode(manUtd);

        assertThat(descendantsForNode).isNotNull().isEmpty();
    }

    @Test
    void aNodeWithLeavesChildrenReturnThenAsDescendents() {
        Collection<Node> descendantsForNode = unit.getDescendantsForNode(premierShip);

        assertThat(descendantsForNode).isNotNull()
            .hasSize(2)
            .containsExactlyInAnyOrder(manCity, manUtd);
    }

    @Test
    void anIntermidateNodeShouldReturnAllDescendents() {
        Collection<Node> descendantsForNode = unit.getDescendantsForNode(competition);

        assertThat(descendantsForNode).isNotNull()
            .hasSize(4)
            .containsExactlyInAnyOrder(premierShip, championLeague, manCity, manUtd);
    }

    @Test
    void aRootNodeShouldReturnAllDescendents() {
        Collection<Node> descendantsForNode = unit.getDescendantsForNode(football);

        assertThat(descendantsForNode).isNotNull()
            .hasSize(6)
            .containsExactlyInAnyOrder(competition, player, premierShip, championLeague, manCity, manUtd);
    }

    @Test
    void aRootNodeShouldNotReturnAncestors() {
        Collection<Node> ancestorsForNode = unit.getAncestorsForNode(football);

        assertThat(ancestorsForNode).isNotNull().isEmpty();
    }

    @Test
    void aNodeWithTheRootNodeAsParentReturnASingleAncestor() {
        Collection<Node> ancestorsForNode = unit.getAncestorsForNode(competition);

        assertThat(ancestorsForNode).isNotNull()
            .hasSize(1)
            .containsExactlyInAnyOrder(football);
    }

    @Test
    void anIntermidateNodeShouldReturnAllAncestors() {
        Collection<Node> ancestorsForNode = unit.getAncestorsForNode(premierShip);

        assertThat(ancestorsForNode).isNotNull()
            .hasSize(2)
            .containsExactlyInAnyOrder(competition, football);
    }
}
