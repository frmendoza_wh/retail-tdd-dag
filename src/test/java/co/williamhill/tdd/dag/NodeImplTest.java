package co.williamhill.tdd.dag;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class NodeImplTest {

    @Test
    void shouldCreateANodeWithAName() {
        Node football = new NodeImpl("Football");

        assertThat(football.getName()).isEqualTo("Football");
    }

    @Test
    void shouldAddAChildNodeToAParentNode() {
        Node football = new NodeImpl("Football");
        Node competition = new NodeImpl("Competition");

        football.addChild(competition);

        assertThat(football.getChildren()).isNotNull()
                .isNotEmpty()
                .hasSize(1)
                .containsExactlyInAnyOrder(competition);
    }

    @Test
    void shouldAParentHaveManyChildren() {
        Node competition = new NodeImpl("Competition");
        Node premiership = new NodeImpl("Premiership");
        Node championsLeague = new NodeImpl("Champions League");

        competition.addChild(premiership);
        competition.addChild(championsLeague);

        assertThat(competition.getChildren()).isNotNull()
                .isNotEmpty()
                .hasSize(2)
                .containsExactlyInAnyOrder(premiership, championsLeague);
    }

    @Test
    void shouldNotHaveDuplicatedChildren() {
        Node football = new NodeImpl("Football");
        Node competition = new NodeImpl("Competition");
        Node competition2 = new NodeImpl("Competition");

        football.addChild(competition);
        football.addChild(competition2);

        assertThat(football.getChildren()).isNotNull()
                .isNotEmpty()
                .hasSize(1)
                .containsExactlyInAnyOrder(competition);
    }

    @Test
    void shouldAddParentNodeToAChild() {
        Node football = new NodeImpl("Football");
        Node competition = new NodeImpl("Competition");

        competition.addParent(football);

        assertThat(competition.getParents()).isNotNull()
                .hasSize(1)
                .containsExactlyInAnyOrder(football);
    }

    @Test
    void shouldAChildHaveManyParents() {
        Node manUtd = new NodeImpl("Man Utd");
        Node premiership = new NodeImpl("Premiership");
        Node championsLeague = new NodeImpl("Champions League");

        manUtd.addParent(premiership);
        manUtd.addParent(championsLeague);

        assertThat(manUtd.getParents()).isNotNull()
                .hasSize(2)
                .containsExactlyInAnyOrder(premiership, championsLeague);
    }

    @Test
    void shouldNotHaveDuplicatedParentChildren() {
        Node manUtd = new NodeImpl("Man Utd");
        Node premiership = new NodeImpl("Premiership");
        Node premiership2 = new NodeImpl("Premiership");

        manUtd.addParent(premiership);
        manUtd.addParent(premiership2);

        assertThat(manUtd.getParents()).isNotNull()
                .hasSize(1)
                .containsExactlyInAnyOrder(premiership);
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenAddingAParentThatIsAlreadyAChild() {
        Node competition = new NodeImpl("Competition");
        Node premiership = new NodeImpl("Premiership");

        competition.addChild(premiership);

        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> competition.addParent(premiership));

        assertEquals(NodeImpl.CAN_NOT_ADD_PARENT_MESSAGE, illegalArgumentException.getMessage());
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenAddingAChildThatIsAlreadyAParent() {

        Node football = new NodeImpl("Football");
        Node competition = new NodeImpl("Competition");

        competition.addParent(football);

        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> competition.addChild(football));

        assertEquals(NodeImpl.CAN_NOT_ADD_CHILD_MESSAGE, illegalArgumentException.getMessage());
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenAddingItSelfAsParent() {
        Node football = new NodeImpl("Football");

        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> football.addParent(football));

        assertEquals(NodeImpl.CAN_NOT_ADD_ITSELF, illegalArgumentException.getMessage());
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenAddingItSelfAsChild() {

        Node football = new NodeImpl("Football");

        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> football.addChild(football));

        assertEquals(NodeImpl.CAN_NOT_ADD_ITSELF, illegalArgumentException.getMessage());
    }
}
