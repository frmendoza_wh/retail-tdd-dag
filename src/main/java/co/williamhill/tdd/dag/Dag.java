package co.williamhill.tdd.dag;

import java.util.Collection;

public interface Dag<NODE extends Node> {

    Collection<NODE> getDescendantsForNode(NODE givenNode);
    Collection<NODE> getAncestorsForNode(NODE givenNode);
}
